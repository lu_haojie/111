import tkinter as tk
from tkinter import messagebox
from tkinter import ttk

# 房产信息类
class Property:
    def __init__(self, prop_id, name, address):
        self.prop_id = prop_id
        self.name = name
        self.address = address

    def __str__(self):
        return f"房产ID: {self.prop_id}, 名称: {self.name}, 地址: {self.address}"

# 收费记录类
class ChargeRecord:
    def __init__(self, record_id, date, amount, description):
        self.record_id = record_id
        self.date = date
        self.amount = amount
        self.description = description

    def __str__(self):
        return f"记录ID: {self.record_id}, 日期: {self.date}, 金额: {self.amount}, 描述: {self.description}"

# 文件管理类
class Document:
    def __init__(self, doc_id, name, file_type):
        self.doc_id = doc_id
        self.name = name
        self.file_type = file_type

    def __str__(self):
        return f"文件ID: {self.doc_id}, 名称: {self.name}, 类型: {self.file_type}"

# 停车场类
class ParkingLot:
    def __init__(self, lot_id, name, location):
        self.lot_id = lot_id
        self.name = name
        self.location = location

    def __str__(self):
        return f"停车场ID: {self.lot_id}, 名称: {self.name}, 地点: {self.location}"

# 保洁工信息类
class Cleaner:
    def __init__(self, cleaner_id, name, contact):
        self.cleaner_id = cleaner_id
        self.name = name
        self.contact = contact

    def __str__(self):
        return f"保洁工ID: {self.cleaner_id}, 姓名: {self.name}, 联系方式: {self.contact}"

# 保安信息类
class Security:
    def __init__(self, security_id, name, department):
        self.security_id = security_id
        self.name = name
        self.department = department

    def __str__(self):
        return f"保安ID: {self.security_id}, 姓名: {self.name}, 部门: {self.department}"

# 登录界面类
class LoginApp:
    def __init__(self, root, switch_to_main):
        self.root = root
        self.switch_to_main = switch_to_main
        self.root.title("登录")

        self.label_username = tk.Label(root, text="用户名:")
        self.label_username.grid(row=0, column=0, padx=10, pady=5)
        self.entry_username = tk.Entry(root)
        self.entry_username.grid(row=0, column=1, padx=10, pady=5)

        self.label_password = tk.Label(root, text="密码:")
        self.label_password.grid(row=1, column=0, padx=10, pady=5)
        self.entry_password = tk.Entry(root, show="*")
        self.entry_password.grid(row=1, column=1, padx=10, pady=5)

        self.button_login = tk.Button(root, text="登录", command=self.login)
        self.button_login.grid(row=2, column=0, columnspan=2, padx=10, pady=5)

    def login(self):
        username = self.entry_username.get()
        password = self.entry_password.get()

        # 这里可以添加实际的用户验证逻辑
        if username == "高文琦" and password == "123456":
            messagebox.showinfo("提示", "登录成功")
            self.switch_to_main()
        else:
            messagebox.showerror("错误", "用户名或密码错误")

# 业务管理主界面类
class BusinessManagementApp:
    def __init__(self, root, return_to_login):
        self.root = root
        self.return_to_login = return_to_login
        self.root.title("业务管理系统")

        # 初始化数据
        self.properties = {}
        self.charge_records = []
        self.documents = {}
        self.parking_lots = {}
        self.cleaners = {}
        self.securities = {}

        # 创建界面组件
        self.create_widgets()

    def create_widgets(self):
        # 标签和按钮
        self.label_info = tk.Label(self.root, text="请选择操作模块:")
        self.label_info.grid(row=0, column=0, padx=10, pady=5)

        self.treeview = ttk.Treeview(self.root, columns=("ID", "Name", "Type"))
        self.treeview.grid(row=1, column=0, rowspan=6, columnspan=2, padx=10, pady=5, sticky="nsew")
        self.treeview.heading("#0", text="模块")
        self.treeview.heading("ID", text="ID")
        self.treeview.heading("Name", text="名称")
        self.treeview.heading("Type", text="类型")
        self.treeview.bind("<ButtonRelease-1>", self.on_tree_select)

        self.button_create = tk.Button(self.root, text="新建", command=self.create_item)
        self.button_create.grid(row=7, column=0, padx=10, pady=5)

        self.button_update = tk.Button(self.root, text="修改", command=self.update_item)
        self.button_update.grid(row=7, column=1, padx=10, pady=5)

        self.button_delete = tk.Button(self.root, text="删除", command=self.delete_item)
        self.button_delete.grid(row=8, column=0, columnspan=2, padx=10, pady=5)

        # 返回主页按钮
        self.button_home = tk.Button(self.root, text="返回主页", command=self.return_to_login)
        self.button_home.grid(row=9, column=0, columnspan=2, padx=10, pady=5)

        # 填充模拟数据
        self.fill_data()

    def return_to_login(self):
        # 销毁当前所有小部件
        for widget in self.root.winfo_children():
            widget.destroy()

        # 重新创建登录界面
        login_app = LoginApp(self.root, self.switch_to_main)

    def fill_data(self):
        # 填充模拟数据
        self.treeview.insert("", "end", text="房产管理", values=("1", "房产管理", "信息类"))
        self.treeview.insert("", "end", text="收费管理", values=("2", "收费管理", "记录类"))
        self.treeview.insert("", "end", text="文档管理", values=("3", "文档管理", "信息类"))
        self.treeview.insert("", "end", text="停车管理", values=("4", "停车管理", "信息类"))
        self.treeview.insert("", "end", text="保洁管理", values=("5", "保洁管理", "信息类"))
        self.treeview.insert("", "end", text="保安管理", values=("6", "保安管理", "信息类"))

        # 初始化具体数据（模拟）
        self.properties = {
            "1": Property("1", "房产A", "地址A"),
            "2": Property("2", "房产B", "地址B")
        }

        self.charge_records = [
            ChargeRecord("1", "2024-01-01", 1000, "物业费"),
            ChargeRecord("2", "2024-01-02", 1500, "水电费")
        ]

        self.documents = {
            "3": Document("3", "文档A", "PDF"),
            "4": Document("4", "文档B", "Word")
        }

        self.parking_lots = {
            "4": ParkingLot("4", "停车场A", "位置A"),
            "5": ParkingLot("5", "停车场B", "位置B")
        }

        self.cleaners = {
            "5": Cleaner("5", "保洁工A", "电话A"),
            "6": Cleaner("6", "保洁工B", "电话B")
        }

        self.securities = {
            "6": Security("6", "保安A", "部门A"),
            "7": Security("7", "保安B", "部门B")
        }

    def on_tree_select(self, event):
        selected_item = self.treeview.selection()[0]
        item_text = self.treeview.item(selected_item, "text")
        if item_text == "房产管理":
            self.show_properties()
        elif item_text == "收费管理":
            self.show_charge_records()
        elif item_text == "文档管理":
            self.show_documents()
        elif item_text == "停车管理":
            self.show_parking_lots()
        elif item_text == "保洁管理":
            self.show_cleaners()
        elif item_text == "保安管理":
            self.show_securities()

    def show_properties(self):
        self.clear_treeview()
        for prop_id, prop in self.properties.items():
            self.treeview.insert("", "end", text=f"房产ID: {prop.prop_id}",
                                 values=(prop.prop_id, prop.name, "房产信息"))

    def show_charge_records(self):
        self.clear_treeview()
        for record in self.charge_records:
            self.treeview.insert("", "end", text=f"记录ID: {record.record_id}",
                                 values=(record.record_id, record.description, "收费记录"))

    def show_documents(self):
        self.clear_treeview()
        for doc_id, doc in self.documents.items():
            self.treeview.insert("", "end", text=f"文件ID: {doc.doc_id}", values=(doc.doc_id, doc.name, "文档"))

    def show_parking_lots(self):
        self.clear_treeview()
        for lot_id, lot in self.parking_lots.items():
            self.treeview.insert("", "end", text=f"停车场ID: {lot.lot_id}", values=(lot.lot_id, lot.name, "停车场"))

    def show_cleaners(self):
        self.clear_treeview()
        for cleaner_id, cleaner in self.cleaners.items():
            self.treeview.insert("", "end", text=f"保洁工ID: {cleaner.cleaner_id}",
                                 values=(cleaner.cleaner_id, cleaner.name, "保洁工"))

    def show_securities(self):
        self.clear_treeview()
        for security_id, security in self.securities.items():
            self.treeview.insert("", "end", text=f"保安ID: {security.security_id}",
                                 values=(security.security_id, security.name, "保安"))

    def create_item(self):
        selected_item = self.treeview.selection()[0]
        item_text = self.treeview.item(selected_item, "text")
        if "房产" in item_text:
            self.create_property()
        elif "收费记录" in item_text:
            self.create_charge_record()
        elif "文档" in item_text:
            self.create_document()
        elif "停车场" in item_text:
            self.create_parking_lot()
        elif "保洁工" in item_text:
            self.create_cleaner()
        elif "保安" in item_text:
            self.create_security()

    def create_property(self):
        prop_id = len(self.properties) + 1
        name = f"房产{prop_id}"
        address = f"地址{prop_id}"
        new_property = Property(str(prop_id), name, address)
        self.properties[str(prop_id)] = new_property
        messagebox.showinfo("提示", f"创建成功: {new_property}")

    def create_charge_record(self):
        record_id = len(self.charge_records) + 1
        date = "2024-01-01"
        amount = 1000
        description = f"费用{record_id}"
        new_record = ChargeRecord(str(record_id), date, amount, description)
        self.charge_records.append(new_record)
        messagebox.showinfo("提示", f"创建成功: {new_record}")

    def create_document(self):
        doc_id = len(self.documents) + 1
        name = f"文档{doc_id}"
        file_type = "PDF"
        new_document = Document(str(doc_id), name, file_type)
        self.documents[str(doc_id)] = new_document
        messagebox.showinfo("提示", f"创建成功: {new_document}")

    def create_parking_lot(self):
        lot_id = len(self.parking_lots) + 1
        name = f"停车场{lot_id}"
        location = f"位置{lot_id}"
        new_parking_lot = ParkingLot(str(lot_id), name, location)
        self.parking_lots[str(lot_id)] = new_parking_lot
        messagebox.showinfo("提示", f"创建成功: {new_parking_lot}")

    def create_cleaner(self):
        cleaner_id = len(self.cleaners) + 1
        name = f"保洁工{cleaner_id}"
        contact = f"电话{cleaner_id}"
        new_cleaner = Cleaner(str(cleaner_id), name, contact)
        self.cleaners[str(cleaner_id)] = new_cleaner
        messagebox.showinfo("提示", f"创建成功: {new_cleaner}")

    def create_security(self):
        security_id = len(self.securities) + 1
        name = f"保安{security_id}"
        department = f"部门{security_id}"
        new_security = Security(str(security_id), name, department)
        self.securities[str(security_id)] = new_security
        messagebox.showinfo("提示", f"创建成功: {new_security}")

    def update_item(self):
        selected_item = self.treeview.selection()[0]
        item_text = self.treeview.item(selected_item, "text")
        messagebox.showinfo("提示", f"暂不支持修改: {item_text}")

    def delete_item(self):
        selected_item = self.treeview.selection()[0]
        item_text = self.treeview.item(selected_item, "text")
        messagebox.showinfo("提示", f"暂不支持删除: {item_text}")

    def clear_treeview(self):
        for item in self.treeview.get_children():
            self.treeview.delete(item)

# 主程序入口
if __name__ == "__main__":
    root = tk.Tk()

    def switch_to_main():
        for widget in root.winfo_children():
            widget.destroy()
        app = BusinessManagementApp(root, switch_to_main)

    login_app = LoginApp(root, switch_to_main)
    root.mainloop()
