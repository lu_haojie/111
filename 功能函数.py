class Property:
    def __init__(self, property_id, property_name, address):
        self.property_id = property_id
        self.property_name = property_name
        self.address = address
        self.buildings = []  # 楼宇列表
    def add_building(self, building):
        self.buildings.append(building)
    def __str__(self):
        return f"物业ID: {self.property_id}\n物业名称: {self.property_name}\n地址: {self.address}\n"
class Building:
    def __init__(self, building_id, building_number, property_id):
        self.building_id = building_id
        self.building_number = building_number
        self.property_id = property_id
        self.rooms = []  # 房间列表
    def add_room(self, room):
        self.rooms.append(room)
    def __str__(self):
        return f"楼宇ID: {self.building_id}\n楼号: {self.building_number}\n物业ID: {self.property_id}\n"
class Room:
    def __init__(self, room_id, room_number, building_id):
        self.room_id = room_id
        self.room_number = room_number
        self.building_id = building_id
    def __str__(self):
        return f"房间ID: {self.room_id}\n房间号: {self.room_number}\n楼宇ID: {self.building_id}\n"
class Document:
    def __init__(self, document_id, title, category):
        self.document_id = document_id
        self.title = title
        self.category = category
    def __str__(self):
        return f"文档ID: {self.document_id}\n标题: {self.title}\n分类: {self.category}\n"
class ParkingLot:
    def __init__(self, lot_id, lot_number, property_id):
        self.lot_id = lot_id
        self.lot_number = lot_number
        self.property_id = property_id
    def __str__(self):
        return f"停车位ID: {self.lot_id}\n停车位编号: {self.lot_number}\n物业ID: {self.property_id}\n"
class Cleaner:
    def __init__(self, cleaner_id, cleaner_name, team):
        self.cleaner_id = cleaner_id
        self.cleaner_name = cleaner_name
        self.team = team
    def __str__(self):
        return f"保洁员ID: {self.cleaner_id}\n保洁员姓名: {self.cleaner_name}\n班组: {self.team}\n"
class SecurityGuard:
    def __init__(self, guard_id, guard_name, team):
        self.guard_id = guard_id
        self.guard_name = guard_name
        self.team = team
    def __str__(self):
        return f"保安员ID: {self.guard_id}\n保安员姓名: {self.guard_name}\n班组: {self.team}\n"
class ManagementSystem:
    def __init__(self):
        self.properties = []       # 物业列表
        self.documents = []        # 文档列表
        self.parking_lots = []     # 停车位列表
        self.cleaners = []         # 保洁员列表
        self.security_guards = []  # 保安员列表
    # 添加物业信息
    def add_property(self, property_id, property_name, address):
        property_obj = Property(property_id, property_name, address)
        self.properties.append(property_obj)
        return property_obj
    # 列出所有物业信息
    def list_properties(self):
        return self.properties
    # 根据ID查找物业信息
    def find_property_by_id(self, property_id):
        for property in self.properties:
            if property.property_id == property_id:
                return property
        return None
    # 添加楼宇到指定物业
    def add_building_to_property(self, property_id, building_id, building_number):
        property = self.find_property_by_id(property_id)
        if property:
            building_obj = Building(building_id, building_number, property_id)
            property.add_building(building_obj)
            return building_obj
        return None
    # 添加房间到指定楼宇
    def add_room_to_building(self, building_id, room_id, room_number):
        for property in self.properties:
            for building in property.buildings:
                if building.building_id == building_id:
                    room_obj = Room(room_id, room_number, building_id)
                    building.add_room(room_obj)
                    return room_obj
        return None
    # 添加文档信息
    def add_document(self, document_id, title, category):
        document_obj = Document(document_id, title, category)
        self.documents.append(document_obj)
        return document_obj
    # 列出所有文档信息
    def list_documents(self):
        return self.documents
    # 添加停车位信息
    def add_parking_lot(self, lot_id, lot_number, property_id):
        parking_lot_obj = ParkingLot(lot_id, lot_number, property_id)
        self.parking_lots.append(parking_lot_obj)
        return parking_lot_obj
    # 列出所有停车位信息
    def list_parking_lots(self):
        return self.parking_lots
    # 添加保洁员信息
    def add_cleaner(self, cleaner_id, cleaner_name, team):
        cleaner_obj = Cleaner(cleaner_id, cleaner_name, team)
        self.cleaners.append(cleaner_obj)
        return cleaner_obj
    # 列出所有保洁员信息
    def list_cleaners(self):
        return self.cleaners
    # 添加保安员信息
    def add_security_guard(self, guard_id, guard_name, team):
        guard_obj = SecurityGuard(guard_id, guard_name, team)
        self.security_guards.append(guard_obj)
        return guard_obj
    # 列出所有保安员信息
    def list_security_guards(self):
        return self.security_guards
# 示例用法
if __name__ == "__main__":
    management_system = ManagementSystem()
    # 添加物业信息
    property1 = management_system.add_property(1, "日落公寓", "123 Sunset Blvd")
    property2 = management_system.add_property(2, "河滨公寓", "456 Riverside Ave")
    # 添加楼宇信息
    building1 = management_system.add_building_to_property(1, 101, "A栋")
    building2 = management_system.add_building_to_property(2, 201, "B栋")
    # 添加房间信息
    room1 = management_system.add_room_to_building(101, 1001, "1001房")
    room2 = management_system.add_room_to_building(201, 2001, "2001房")
    # 添加文档信息
    doc1 = management_system.add_document(1, "租赁协议", "法律文件")
    doc2 = management_system.add_document(2, "维护清单", "运营文件")
    # 添加停车位信息
    parking1 = management_system.add_parking_lot(1, "A101", property1.property_id)
    parking2 = management_system.add_parking_lot(2, "B202", property2.property_id)
    # 添加保洁员信息
    cleaner1 = management_system.add_cleaner(1, "艾丽斯", "A班组")
    cleaner2 = management_system.add_cleaner(2, "鲍勃", "B班组")
    # 添加保安员信息
    guard1 = management_system.add_security_guard(1, "约翰", "X班组")
    guard2 = management_system.add_security_guard(2, "伊娃", "Y班组")
    # 打印物业信息列表
    print("物业信息:")
    for property in management_system.list_properties():
        print(property)
        for building in property.buildings:
            print(building)
            for room in building.rooms:
                print(room)
    # 打印文档信息列表
    print("文档信息:")
    for document in management_system.list_documents():
        print(document)
    # 打印停车位信息列表
    print("停车位信息:")
    for parking_lot in management_system.list_parking_lots():
        print(parking_lot)
    # 打印保洁员信息列表
    print("保洁员信息:")
    for cleaner in management_system.list_cleaners():
        print(cleaner)
    # 打印保安员信息列表
    print("保安员信息:")
    for guard in management_system.list_security_guards():
        print(guard)
