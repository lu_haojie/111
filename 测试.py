import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QLineEdit, QTextEdit, QLabel, \
    QMessageBox


class PropertyManagementApp(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.properties = {}  # 简单起见,使用字典存储房产信息

    def initUI(self):
        self.setWindowTitle('房产信息管理')
        self.setGeometry(100, 100, 600, 400)

        layout = QVBoxLayout()

        # 查询区域
        search_layout = QHBoxLayout()
        self.search_input = QLineEdit()
        search_button = QPushButton('查询小区下的房产')
        search_button.clicked.connect(self.search_properties)
        search_layout.addWidget(QLabel('小区名称:'))
        search_layout.addWidget(self.search_input)
        search_layout.addWidget(search_button)
        layout.addLayout(search_layout)

        # 显示区域
        self.display_area = QTextEdit()
        self.display_area.setReadOnly(True)
        layout.addWidget(self.display_area)

        # 新建房产区域
        add_layout = QHBoxLayout()
        self.community_input = QLineEdit()
        self.property_input = QLineEdit()
        add_button = QPushButton('新建房产')
        add_button.clicked.connect(self.add_property)
        add_layout.addWidget(QLabel('小区名称:'))
        add_layout.addWidget(self.community_input)
        add_layout.addWidget(QLabel('房产信息:'))
        add_layout.addWidget(self.property_input)
        add_layout.addWidget(add_button)
        layout.addLayout(add_layout)

        # 修改和删除区域
        modify_delete_layout = QHBoxLayout()
        self.modify_input = QLineEdit()
        modify_button = QPushButton('修改房产信息')
        modify_button.clicked.connect(self.modify_property)
        delete_button = QPushButton('删除房产')
        delete_button.clicked.connect(self.delete_property)
        modify_delete_layout.addWidget(QLabel('房产ID:'))
        modify_delete_layout.addWidget(self.modify_input)
        modify_delete_layout.addWidget(modify_button)
        modify_delete_layout.addWidget(delete_button)
        layout.addLayout(modify_delete_layout)

        self.setLayout(layout)

    def search_properties(self):
        community = self.search_input.text()
        if community in self.properties:
            self.display_area.setText(f"{community}的房产列表:\n" + "\n".join(self.properties[community]))
        else:
            self.display_area.setText(f"未找到{community}的房产信息")

    def add_property(self):
        community = self.community_input.text()
        property_info = self.property_input.text()
        if community and property_info:
            if community not in self.properties:
                self.properties[community] = []
            self.properties[community].append(property_info)
            QMessageBox.information(self, "成功", "房产添加成功")
            self.community_input.clear()
            self.property_input.clear()
        else:
            QMessageBox.warning(self, "错误", "请输入完整的小区名称和房产信息")

    def modify_property(self):
        property_id = self.modify_input.text()
        # 这里简化处理,实际应用中需要更复杂的逻辑
        QMessageBox.information(self, "提示", f"修改房产 {property_id} 的信息")

    def delete_property(self):
        property_id = self.modify_input.text()
        # 这里简化处理,实际应用中需要更复杂的逻辑
        QMessageBox.information(self, "提示", f"删除房产 {property_id}")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = PropertyManagementApp()
    ex.show()
    sys.exit(app.exec_())
